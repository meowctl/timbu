package timbu

import (
	"fmt"
	"net/http"
	"strings"
	"sync"
	"sync/atomic"
)

type DownloadInfo struct {
	downloaded atomic.Int64

	done chan struct{}
	once sync.Once
}

func NewDownloadInfo() *DownloadInfo {
	return &DownloadInfo{done: make(chan struct{})}
}

func (d *DownloadInfo) Downloaded() int64 { return d.downloaded.Load() }

func (d *DownloadInfo) Inc(value int64) int64 { return d.downloaded.Add(value) }

func (d *DownloadInfo) Done() <-chan struct{} { return d.done }

func (d *DownloadInfo) SetDone() {
	d.once.Do(func() { close(d.done) })
}

type RetryCount int

func (rc RetryCount) Exceeds(that RetryCount) bool {
	// less than 0: infinite retries
	return rc >= 0 && that >= 0 && rc >= that
}

type Retry struct {
	Status, Connection RetryCount
}

type RequestError struct {
	Request  *http.Request
	Response *http.Response
	err      error
}

func (e RequestError) Error() string {
	method, url := e.Request.Method, e.Request.URL.String()
	switch {
	case url == "":
		return e.err.Error()
	case method == "":
		method = "Get"
	default:
		// GET -> Get
		method = strings.ToUpper(method[0:1]) + strings.ToLower(method[1:])
	}
	return fmt.Sprintf(`%s "%s": %v`, method, url, e.err)
}

func (e RequestError) Unwrap() error { return e.err }

type StatusCodeError struct {
	Code int
}

func (e StatusCodeError) Error() string {
	return fmt.Sprintf("server returned %d status", e.Code)
}

type IncompleteReadError struct {
	ExpectedLength, ActualLength int64
}

func (e IncompleteReadError) Error() string {
	return fmt.Sprintf("unexpected EOF: expected length: %d, actual length: %d", e.ExpectedLength, e.ActualLength)
}
