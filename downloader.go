package timbu

import (
	"cmp"
	"context"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"slices"
	"time"

	"codeberg.org/meowctl/timbu/internal"
)

const (
	MinRetrySleepDuration = time.Duration(0.2 * float64(time.Second))
	MaxRetrySleepDuration = time.Duration(1.5 * float64(time.Second))
)

func retrySleep(ctx context.Context, retries int) error {
	d := min(MinRetrySleepDuration*time.Duration(retries), MaxRetrySleepDuration)
	select {
	case <-time.After(d):
	case <-ctx.Done():
		return ctx.Err()
	}
	return nil
}

const (
	ChunkSize int64 = 16384 // 16kb

	MinJobTakeoverSize    int64 = 1048576 // 1mb
	EndJobTakeoverSize    int64 = MinJobTakeoverSize / 3 * 2
	MinEndJobTakeoverSize int64 = MinJobTakeoverSize / 3

	DefaultTimeout = 10 * time.Second
	DefaultWorkers = 5
)

var (
	DefaultClient = internal.NewHTTPClient(DefaultTimeout, true)
	DefaultRetry  = Retry{Status: 2, Connection: 6}
)

type Download struct {
	URL            string
	DownloadInfo   *DownloadInfo
	supportsRanges bool
}

func NewDownload(url string) Download {
	return Download{URL: url, DownloadInfo: NewDownloadInfo()}
}

type Downloader struct {
	Client  *http.Client
	Header  http.Header
	Retry   Retry
	workers int
}

func NewDownloader(client *http.Client, retry Retry, workers int) *Downloader {
	if workers < 1 {
		panic("'workers' must not be less than 1")
	}
	return &Downloader{
		Client:  client,
		Header:  make(http.Header),
		Retry:   retry,
		workers: workers,
	}
}

func NewDownloaderWithDefaults() *Downloader {
	return NewDownloader(DefaultClient, DefaultRetry, DefaultWorkers)
}

func (d *Downloader) Workers() int { return d.workers }

type RequestInfo struct {
	Retry Retry
	resp  *http.Response
}

func (r RequestInfo) Response() *http.Response { return r.resp }

func (r RequestInfo) Length() int64 { return r.resp.ContentLength }

func (r RequestInfo) Close() error { return r.resp.Body.Close() }

type InitialRequest struct {
	RequestInfo
	ctx    context.Context
	cancel context.CancelFunc
}

func (d *Downloader) SendInitialRequest(ctx context.Context, download Download) (InitialRequest, error) {
	reqCtx, cancel := context.WithCancel(ctx)
	reqInfo, err := d.get(reqCtx, newRequest(download.URL, Retry{}))
	if err != nil {
		cancel()
		err = fmt.Errorf("initial request: %w", err)
	}
	return InitialRequest{reqInfo, reqCtx, cancel}, err
}

func (ir InitialRequest) Cancel() context.CancelFunc { return ir.cancel }

func (ir InitialRequest) Close() error {
	defer ir.cancel()
	return ir.RequestInfo.Close()
}

type downloadJob struct {
	download Download
	workerId int

	reqInfo RequestInfo
	dlInfo  *DownloadInfo

	file     *os.File
	writeSem internal.Semaphore

	off, relativeLen int64
	relativeLenCh    chan int64
}

func (job *downloadJob) relativeOffset() int64 {
	return job.relativeLen - job.off - job.dlInfo.Downloaded()
}

func (job *downloadJob) writeAt(ctx context.Context, b []byte, off int64) (n int, err error) {
	if err = job.writeSem.Acquire(ctx); err != nil {
		return
	}
	defer job.writeSem.Release()
	return job.file.WriteAt(b, off)
}

func (job *downloadJob) readBody(ctx context.Context) error {
	defer job.reqInfo.Close()

	type readResult struct {
		n   int
		err error
	}

	var (
		buf     = make([]byte, ChunkSize)
		eventCh = make(chan struct{}, 1)
		resCh   = make(chan readResult, 1)
	)
	go func() {
		for range eventCh {
			n, err := io.ReadFull(job.reqInfo.resp.Body, buf)
			resCh <- readResult{n, err}
		}
	}()
	defer close(eventCh)

	var (
		initialOffset = job.off

		eof = false
	)
	for (job.off < job.relativeLen || job.relativeLen < 0) && !eof {
		eventCh <- struct{}{}
		var (
			res      readResult
			received = false
		)
		for !received {
			select {
			case res = <-resCh:
				received = true
			case job.relativeLen = <-job.relativeLenCh: // update length if 'taken over'
			}
		}
		if res.err != nil {
			eof = errors.Is(res.err, io.EOF) || errors.Is(res.err, io.ErrUnexpectedEOF)
			if !eof {
				return res.err
			}
		}

		bytesRead := int64(res.n)
		if job.relativeLen > 0 && job.off+bytesRead > job.relativeLen {
			bytesRead = max(0, job.relativeLen-job.off)
		}
		if _, err := job.writeAt(ctx, buf[:bytesRead], job.off); err != nil {
			return err
		}
		job.download.DownloadInfo.Inc(bytesRead)
		job.dlInfo.Inc(bytesRead)
		job.off += bytesRead

		if eof && job.off < job.relativeLen {
			return &RequestError{
				Request:  job.reqInfo.resp.Request,
				Response: job.reqInfo.resp,
				err:      &IncompleteReadError{job.relativeLen - initialOffset, job.off - initialOffset},
			}
		}
	}
	return nil
}

type downloadResult struct {
	job downloadJob
	err error
}

func (d *Downloader) Run(f *os.File, initialReq InitialRequest, download Download) error {
	if d.workers < 1 {
		panic("no workers available: Downloader.workers is set to less than 1")
	}

	var (
		ctx, cancel = initialReq.ctx, initialReq.cancel
		initialResp = initialReq.resp
	)
	defer initialReq.Close()

	download.supportsRanges = initialResp.Header.Get("Accept-Ranges") == "bytes"
	switch {
	case !download.supportsRanges:
		slog.InfoContext(ctx, "server does not support partial requests")
	case initialResp.ContentLength == -1:
		slog.InfoContext(ctx, "content-length header invalid or unavailable")
	}

	var (
		activeWorkers   = 1
		workerRangeSize = initialResp.ContentLength
	)
	if download.supportsRanges && initialResp.ContentLength > 0 {
		// ContentLength=5, d.workers=10   -> activeWorkers=5
		// ContentLength=5, ChunkSize=8192 -> activeWorkers=1
		activeWorkers = int(min(
			min(int64(d.workers), initialResp.ContentLength),
			max(1, initialResp.ContentLength/ChunkSize)))
		workerRangeSize = initialResp.ContentLength / int64(activeWorkers)
	}

	var (
		jobs     = make([]downloadJob, 0, activeWorkers)
		writeSem = internal.NewSemaphore(1)
	)
	for off := int64(0); len(jobs) < activeWorkers; off += workerRangeSize {
		job := downloadJob{
			download: download,
			workerId: len(jobs),

			reqInfo: initialReq.RequestInfo,
			dlInfo:  NewDownloadInfo(),

			file: f, writeSem: writeSem,
			off: off, relativeLen: off + workerRangeSize,
			relativeLenCh: make(chan int64),
		}
		if len(jobs) > 0 {
			job.reqInfo = RequestInfo{}
		}
		jobs = append(jobs, job)
	}
	jobs[len(jobs)-1].relativeLen = initialResp.ContentLength

	resCh := make(chan downloadResult)
	for _, job := range jobs {
		go d.downloadWorker(ctx, cancel, job, resCh)
	}

	// stores the first non-nil error returned by a worker
	var workerErr error

	for jobResult := range resCh {
		activeWorkers--

		switch {
		case ctx.Err() != nil || jobResult.err != nil:
		case activeWorkers > 0:
			if d.takeoverJob(ctx, jobResult.job, jobs) {
				go d.downloadWorker(ctx, cancel, jobs[jobResult.job.workerId], resCh)
				activeWorkers++
			}
			continue
		}

		workerErr = jobResult.err
		for ; 0 < activeWorkers; activeWorkers-- {
			jobResult = <-resCh
			if workerErr == nil {
				workerErr = jobResult.err
			}
		}
		break
	}
	if workerErr != nil {
		return workerErr
	}

	download.DownloadInfo.SetDone()
	return nil
}

func (d *Downloader) takeoverJob(ctx context.Context, doneJob downloadJob, jobs []downloadJob) (ok bool) {
	var (
		job = slices.MaxFunc(jobs, func(a, b downloadJob) int {
			return cmp.Compare(a.relativeOffset(), b.relativeOffset())
		})
		initialRelativeLen = job.relativeLen
		remainingHalf      = job.relativeOffset() / 2
	)
	if remainingHalf < MinJobTakeoverSize && remainingHalf > EndJobTakeoverSize ||
		remainingHalf < MinEndJobTakeoverSize {
		return false
	}
	job.relativeLen -= remainingHalf
	select {
	case job.relativeLenCh <- job.relativeLen:
		doneJob.reqInfo = RequestInfo{}
		doneJob.dlInfo = NewDownloadInfo()

		doneJob.off = max(job.relativeLen, job.off+job.dlInfo.Downloaded())
		doneJob.relativeLen = initialRelativeLen

		jobs[job.workerId] = job
		jobs[doneJob.workerId] = doneJob
		return true
	case <-job.dlInfo.Done():
	case <-ctx.Done():
	}
	return false
}

func (d *Downloader) downloadWorker(
	ctx context.Context, cancel context.CancelFunc,
	job downloadJob, resCh chan<- downloadResult,
) {
	if err := d.startDownload(ctx, job); err != nil {
		// if err wasn't caused by ctx being canceled, log it
		if !errors.Is(err, ctx.Err()) {
			slog.ErrorContext(ctx, fmt.Sprintf("download worker %d: %v", job.workerId, err))
		}
		resCh <- downloadResult{job, fmt.Errorf("download: %w", err)}
		cancel()
		return
	}
	job.dlInfo.SetDone()
	resCh <- downloadResult{job, nil}
}

func (d *Downloader) startDownload(ctx context.Context, job downloadJob) error {
	for retries := 0; ; retries++ {
		if job.reqInfo.resp == nil || retries > 0 {
			if err := retrySleep(ctx, retries); err != nil {
				return err
			}

			var (
				err error
				req = newRequest(job.download.URL, job.reqInfo.Retry)
			)
			switch {
			case job.relativeLen < 0 || !job.download.supportsRanges:
				job.reqInfo, err = d.get(ctx, req)
			default:
				job.reqInfo, err = d.getPartial(ctx, req, job.off, job.relativeLen)
			}
			if err != nil {
				return err
			}
		}

		err := job.readBody(ctx)
		if err == nil {
			return nil
		}
		err = fmt.Errorf("response body read: %w", err)
		if !isRetryable(ctx, err) || job.reqInfo.Retry.Connection.Exceeds(d.Retry.Connection) ||
			(job.relativeLen < 0 || !job.download.supportsRanges) && job.dlInfo.Downloaded() > 0 {
			return err
		}
		slog.ErrorContext(ctx, fmt.Sprint(err))
		job.reqInfo.Retry.Connection++
	}
}

type request struct {
	url    string
	header http.Header
	retry  Retry
}

func newRequest(url string, retry Retry) request {
	return request{
		url:    url,
		header: make(http.Header),
		retry:  retry,
	}
}

func (d *Downloader) newHTTPRequest(ctx context.Context, method string, req request) (*http.Request, error) {
	httpReq, err := http.NewRequestWithContext(ctx, method, req.url, nil)
	if err != nil {
		return httpReq, err
	}
	addHeaders(httpReq, d.Header)
	addHeaders(httpReq, req.header)
	return httpReq, nil
}

func (d *Downloader) sendRequest(ctx context.Context, req request) (*http.Response, error) {
	slog.InfoContext(ctx, fmt.Sprintf(`Get "%s"`, req.url))
	httpReq, err := d.newHTTPRequest(ctx, "GET", req)
	if err != nil {
		return nil, err
	}
	return d.Client.Do(httpReq)
}

func (d *Downloader) get(ctx context.Context, req request) (RequestInfo, error) {
	var (
		reqInfo = RequestInfo{Retry: req.retry}
		err     error
	)
	for retries := 0; ; retries++ {
		if err := retrySleep(ctx, retries); err != nil {
			return reqInfo, err
		}

		reqInfo.resp, err = d.sendRequest(ctx, req)
		switch {
		case err != nil:
			if !isRetryable(ctx, err) || reqInfo.Retry.Connection.Exceeds(d.Retry.Connection) {
				return reqInfo, err
			}
			reqInfo.Retry.Connection++
		case reqInfo.resp.StatusCode >= 400:
			reqInfo.Close()
			err = &RequestError{
				Request:  reqInfo.resp.Request,
				Response: reqInfo.resp,
				err:      &StatusCodeError{reqInfo.resp.StatusCode},
			}
			if reqInfo.Retry.Status.Exceeds(d.Retry.Status) {
				return reqInfo, err
			}
			reqInfo.Retry.Status++
		default:
			return reqInfo, nil
		}
		slog.ErrorContext(ctx, fmt.Sprint(err))
	}
}

func (d *Downloader) getPartial(ctx context.Context, req request, off, relativeLen int64) (RequestInfo, error) {
	req.header.Add("Range", fmt.Sprintf("bytes=%d-%d", off, relativeLen-1))
	return d.get(ctx, req)
}

func addHeaders(req *http.Request, headers http.Header) {
	for k, values := range headers {
		for _, v := range values {
			req.Header.Add(k, v)
		}
	}
}

var protoRegexp = regexp.MustCompile(`^https?://`)

func isRetryable(ctx context.Context, err error) bool {
	var (
		urlError *url.Error
		netError *net.OpError
		reqError *RequestError
	)
	switch {
	case errors.Is(err, ctx.Err()):
	case errors.As(err, &urlError):
		if urlError.Op == "parse" {
			break
		}
		isHTTP := protoRegexp.MatchString(urlError.URL)
		if !isHTTP {
			break
		}
		return true
	case errors.As(err, &netError) || errors.As(err, &reqError):
		return true
	}
	return false
}
