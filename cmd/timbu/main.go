package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/meowctl/timbu/cmd"
	"github.com/spf13/cobra"
)

func main() {
	var (
		ctx, cancel = context.WithCancelCause(context.Background())

		codeCh = make(chan int)
		sigCh  = make(chan os.Signal, 2)
	)
	signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)

	defer func() { os.Exit(<-codeCh) }()
	defer cancel(nil)

	go func() {
		var (
			exitCode    = 1
			sigReceived = false
		)
		for {
			select {
			case sig := <-sigCh:
				switch sig {
				case os.Interrupt:
					exitCode = int(syscall.SIGINT)
				case syscall.SIGTERM:
					exitCode = int(syscall.SIGTERM)
				}
				// exit immediately if a signal is sent twice
				if sigReceived {
					os.Exit(exitCode)
				}
				sigReceived = true
				slog.WarnContext(ctx, "exiting...")
				cancel(fmt.Errorf("interrupted by user"))
			case receivedCode := <-codeCh:
				if !sigReceived {
					exitCode = receivedCode
				}
			case codeCh <- exitCode:
				return
			}
		}
	}()

	cobra.MousetrapHelpText = ""
	if err := cmd.RootCmd.ExecuteContext(ctx); err == nil {
		codeCh <- 0
	}
}
