package cmd

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"strconv"
	"sync"
	"time"

	"codeberg.org/meowctl/timbu"
	"codeberg.org/meowctl/timbu/internal"
	"github.com/spf13/cobra"
)

const MaxWorkers = 1000

type workerNum int

func (w *workerNum) String() string { return strconv.Itoa(int(*w)) }

func (w *workerNum) Set(value string) error {
	workers, err := strconv.Atoi(value)
	switch {
	case err != nil:
		return err
	case workers < 1:
		return fmt.Errorf("must not be less than 1")
	}
	*w = workerNum(workers)
	return nil
}

func (w *workerNum) Type() string { return "int" }

var (
	RootCmd = &cobra.Command{
		Use:   "timbu [flags] url",
		Short: "a fast HTTP downloader",
		Long:  "a fast HTTP downloader that uses multiple connections to download files",
		Args: func(cmd *cobra.Command, args []string) error {
			switch {
			case len(args) < 1:
				return fmt.Errorf("no url was specified")
			case len(args) > 1:
				return fmt.Errorf("multiple downloads are not supported yet")
			}
			return nil
		},
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			cmd.SilenceUsage = true
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			err := run(cmd, args)
			if errors.Is(err, cmd.Context().Err()) {
				return context.Cause(cmd.Context())
			}
			return err
		},
	}

	outputFile  string
	idleTimeout time.Duration
	numWorkers  workerNum = workerNum(timbu.DefaultWorkers)
	capWorkers  bool
	useHTTP2    bool
)

func init() {
	RootCmd.Flags().StringVarP(&outputFile, "output", "o", "", "output file path")
	RootCmd.Flags().DurationVarP(&idleTimeout, "timeout", "t", timbu.DefaultTimeout, "network inactivity timeout")
	RootCmd.Flags().VarP(&numWorkers, "workers", "w", "number of download workers")
	RootCmd.Flags().BoolVarP(&capWorkers, "unlimited-workers", "u", false,
		fmt.Sprintf("use as many workers as you wish! (capped to %d by default)", MaxWorkers))
	RootCmd.Flags().BoolVar(&useHTTP2, "http2", false, "use http/2 if supported")

	RootCmd.MarkFlagRequired("output")
}

func run(cmd *cobra.Command, urls []string) error {
	if numWorkers > MaxWorkers && !capWorkers {
		return fmt.Errorf(
			"won't use more than %d workers by default, set --unlimited-workers if you wish to do so anyway", MaxWorkers)
	}

	var (
		url = urls[0]

		client     = internal.NewHTTPClient(idleTimeout, useHTTP2)
		downloader = timbu.NewDownloader(client, timbu.DefaultRetry, int(numWorkers))

		progressWg  sync.WaitGroup
		ctx, cancel = context.WithCancel(cmd.Context())
	)
	defer progressWg.Wait()
	defer cancel()

	download := timbu.NewDownload(url)
	defer download.DownloadInfo.SetDone()

	initialReq, err := downloader.SendInitialRequest(ctx, download)
	if err != nil {
		return err
	}
	defer initialReq.Close()

	f, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer f.Close()

	if downloader.Workers() > 1 && initialReq.Length() > 0 {
		slog.InfoContext(ctx, "allocating file space:", slog.Int64("size", initialReq.Length()))
		if err := internal.TruncateFile(f, initialReq.Length()); err != nil {
			return err
		}
	}

	progressWg.Add(1)
	go progressLogger(ctx, &progressWg, time.Duration(500*time.Millisecond), download.DownloadInfo)

	return downloader.Run(f, initialReq, download)
}

func progressLogger(ctx context.Context, wg *sync.WaitGroup, interval time.Duration, dlInfo *timbu.DownloadInfo) {
	defer wg.Done()

	var (
		done   = false
		ticker = time.NewTicker(interval)
	)
	defer ticker.Stop()
	for {
		slog.InfoContext(ctx, "progress", slog.Int64("downloaded", dlInfo.Downloaded()))
		if done {
			break
		}
		done = true
		select {
		case <-dlInfo.Done():
		// case <-ctx.Done():
		case <-ticker.C:
			done = false
		}
	}
}
