package internal

import (
	"errors"
	"fmt"
	"os"

	"golang.org/x/sys/unix"
)

func TruncateFile(f *os.File, size int64) error {
	if err := unix.Fallocate(int(f.Fd()), 0, 0, size); err != nil {
		if errors.Is(err, errors.ErrUnsupported) {
			return f.Truncate(size)
		} else {
			return fmt.Errorf("fallocate: %w", err)
		}
	}
	return nil
}
