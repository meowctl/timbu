package internal

import (
	"fmt"
	"log/slog"
	"os"

	"golang.org/x/sys/windows"
)

func TruncateFile(f *os.File, size int64) error {
	if err := windows.DeviceIoControl(windows.Handle(f.Fd()), windows.FSCTL_SET_SPARSE, nil, 0, nil, 0, nil, nil); err != nil {
		slog.Warn(fmt.Sprintf("failed to set file as sparse: %v", err))
	}
	return f.Truncate(size)
}
