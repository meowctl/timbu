//go:build !(windows || linux)

package internal

import (
	"os"
)

func TruncateFile(f *os.File, size int64) error {
	return f.Truncate(size)
}
