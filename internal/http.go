package internal

import (
	"context"
	"crypto/tls"
	"net"
	"net/http"
	"time"
)

type TimeoutConn struct {
	net.Conn
	ReadTimeout, WriteTimeout time.Duration
}

func (c *TimeoutConn) Read(b []byte) (n int, err error) {
	if c.ReadTimeout != 0 {
		if err = c.SetReadDeadline(time.Now().Add(c.ReadTimeout)); err != nil {
			return
		}
	}
	return c.Conn.Read(b)
}

func (c *TimeoutConn) Write(b []byte) (n int, err error) {
	if c.WriteTimeout != 0 {
		if err = c.SetWriteDeadline(time.Now().Add(c.WriteTimeout)); err != nil {
			return
		}
	}
	return c.Conn.Write(b)
}

func NewHTTPClient(idleTimeout time.Duration, useHTTP2 bool) *http.Client {
	var (
		dialer = &net.Dialer{
			Timeout: idleTimeout,
		}
		transport = &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				conn, err := dialer.DialContext(ctx, network, addr)
				return &TimeoutConn{
					Conn:         conn,
					ReadTimeout:  idleTimeout,
					WriteTimeout: idleTimeout,
				}, err
			},
			ForceAttemptHTTP2:     useHTTP2,
			MaxIdleConns:          10,
			IdleConnTimeout:       1 * time.Minute,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		}
	)
	if !useHTTP2 {
		transport.TLSNextProto = make(map[string]func(string, *tls.Conn) http.RoundTripper)
	}
	return &http.Client{
		Transport: transport,
	}
}
