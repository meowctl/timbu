package internal

import "context"

type Semaphore struct {
	semCh chan struct{}
}

func NewSemaphore(n int) Semaphore {
	return Semaphore{make(chan struct{}, n)}
}

func (s Semaphore) Acquire(ctx context.Context) error {
	select {
	case s.semCh <- struct{}{}:
	case <-ctx.Done():
		return ctx.Err()
	}
	return nil
}

func (s Semaphore) Release() {
	select {
	case <-s.semCh:
	default:
	}
}
